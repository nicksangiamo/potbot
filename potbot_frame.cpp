#include <wx/vscroll.h>
#include <list>
#include <string>
#include <cstdio>
#include <boost/bind.hpp>
#include "channel_list_event.h"
#include "chat_window_event.h"
#include "status_bar_event.h"
#include "bn_session_manager.h"
#include "potbot_frame.h"

using std::list;

PotbotFrame::PotbotFrame(Potbot* potbot, const wxString& title,
                         const wxPoint& pos, const wxSize& size)
    : wxFrame(NULL, wxID_ANY, title, pos, size) {
  this->potbot_ = potbot;
  menuFile = new wxMenu;
  menuFile->Append(ID_CONNECT, "&Connect",
                   "Attempt to connect to battle.net");
  menuFile->Append(ID_DISCONNECT, "&Disconnect",
                   "Disconnect from battle.net");
  menuFile->AppendSeparator();
  menuFile->Append(wxID_EXIT);
  menuFile->Enable(ID_DISCONNECT, false);

  wxMenu* menuHelp = new wxMenu;
  menuHelp->Append(wxID_ABOUT);

  wxMenuBar* menuBar = new wxMenuBar;
  menuBar->Append(menuFile, "&File");
  menuBar->Append(menuHelp, "&Help");

  SetMenuBar(menuBar);

  CreateStatusBar();
  SetStatusText("Disconnected");

  wxBoxSizer* main_sizer = new wxBoxSizer(wxVERTICAL);

  wxPanel* send_bar_panel = new wxPanel(this, -1);
  send_bar_panel->SetFont(send_bar_panel->GetFont().Larger());
  wxTextCtrl* send_bar_textctrl = new wxTextCtrl(send_bar_panel, ID_SENDBARTEXT,
                                                 wxT(""),
                                                 wxDefaultPosition,
                                                 wxDefaultSize,
                                                 wxTE_PROCESS_ENTER);
  wxButton* send_bar_button = new wxButton(send_bar_panel, ID_SENDBUTTON,
                                           "Send");
  wxBoxSizer* send_bar_sizer = new wxBoxSizer(wxHORIZONTAL);
  send_bar_sizer->Add(send_bar_textctrl, 1, 0, 0);
  send_bar_sizer->Add(send_bar_button, 0, 0, 0);
  send_bar_panel->SetSizer(send_bar_sizer);

  wxPanel* chat_window_panel = new wxPanel(this, -1);
  chat_window_panel->SetFont(chat_window_panel->GetFont().Larger());
  wxTextCtrl* chat_window_textctrl = new wxTextCtrl(
      chat_window_panel,
      ID_CHATWINDOW,
      wxT(""),
      wxDefaultPosition,
      wxDefaultSize,
      wxTE_MULTILINE | wxTE_READONLY);
  wxTextCtrl* channel_list_textctrl = new wxTextCtrl(
      chat_window_panel,
      ID_CHANNELLIST,
      wxT(""),
      wxDefaultPosition,
      wxDefaultSize,
      wxTE_MULTILINE | wxTE_READONLY | wxHSCROLL);

  wxBoxSizer* chat_window_sizer = new wxBoxSizer(wxHORIZONTAL);
  chat_window_sizer->Add(chat_window_textctrl, 4, wxEXPAND, 0);
  chat_window_sizer->Add(channel_list_textctrl, 1, wxEXPAND, 0);
  chat_window_panel->SetSizer(chat_window_sizer);
                                                         
  main_sizer->Add(chat_window_panel, 1, wxTOP|wxBOTTOM|wxEXPAND, 5);
  main_sizer->Add(send_bar_panel, 0, wxEXPAND,  0);
  SetSizer(main_sizer);
}

wxBEGIN_EVENT_TABLE(PotbotFrame, wxFrame)
 EVT_MENU(ID_CONNECT, PotbotFrame::OnConnect)
 EVT_COMMAND(ID_CONNECTED, myEVT_CONNECTED, PotbotFrame::OnConnected)
 EVT_MENU(ID_DISCONNECT, PotbotFrame::OnDisconnect)
 EVT_COMMAND(ID_DISCONNECTED, myEVT_DISCONNECTED, PotbotFrame::OnDisconnected)
 EVT_MENU(wxID_EXIT, PotbotFrame::OnExit)
 EVT_MENU(wxID_ABOUT, PotbotFrame::OnAbout)
 EVT_CHATWINDOWEVENT(PotbotFrame::OnChatWindowEvent)
 EVT_STATUSBAREVENT(PotbotFrame::OnStatusBarEvent)
 EVT_CHANNELLISTEVENT(PotbotFrame::OnChannelListEvent)
 EVT_BUTTON(ID_SENDBUTTON, PotbotFrame::OnSendButtonEvent)
 EVT_CHAR_HOOK(PotbotFrame::OnKey)
wxEND_EVENT_TABLE()

void PotbotFrame::OnChatWindowEvent(ChatWindowEvent& event) {
  wxString message = event.Message();
  wxTextCtrl* chat_window_textctrl;
  chat_window_textctrl = (wxTextCtrl*) FindWindowById(ID_CHATWINDOW);
  chat_window_textctrl->AppendText(message + "\n");
}

void PotbotFrame::OnStatusBarEvent(StatusBarEvent& event) {
  wxString status = event.Status();
  SetStatusText(status);
}

void PotbotFrame::OnChannelListEvent(ChannelListEvent& event) {
  list<wxString> channel_list = event.ChannelList();
  wxTextCtrl* channel_list_textctrl;
  channel_list_textctrl = (wxTextCtrl*) FindWindowById(ID_CHANNELLIST);
  channel_list_textctrl->Clear();
  //wxString username = event.Username();
  for(wxString username : channel_list) {
    //puts(username.mb_str());
    channel_list_textctrl->AppendText(std::string(username.mb_str()) + "\n");
  }
}

void PotbotFrame::OnSendButtonEvent(wxCommandEvent& event) {
  SendChatMessage();
}

void PotbotFrame::OnKey(wxKeyEvent &event) {
  if(event.GetKeyCode() != WXK_RETURN ||
     FindFocus() != FindWindowById(ID_SENDBARTEXT)) {
    event.Skip();
  } else {
    SendChatMessage();
  }
}

void PotbotFrame::OnExit(wxCommandEvent& WXUNUSED(event)) {
  Close(true);
}

void PotbotFrame::OnAbout(wxCommandEvent& WXUNUSED(event)) {
  wxMessageBox("PoTBoT v1.0",
               "About PoTBoT", wxOK | wxICON_INFORMATION);
}

void PotbotFrame::OnConnect(wxCommandEvent& WXUNUSED(event)) {
  potbot_->Connect();
}

void PotbotFrame::OnConnected(wxCommandEvent& event) {
  menuFile->Enable(ID_CONNECT, false);
  menuFile->Enable(ID_DISCONNECT, true);
  SetStatusText("Connected");
}

void PotbotFrame::OnDisconnect(wxCommandEvent& event) {
  potbot_->Disconnect();
  wxTextCtrl* channel_list_textctrl, * chat_window_textctrl;
  channel_list_textctrl = (wxTextCtrl*) FindWindowById(ID_CHANNELLIST);
  channel_list_textctrl->Clear();
  chat_window_textctrl = (wxTextCtrl*) FindWindowById(ID_CHATWINDOW);
  chat_window_textctrl->AppendText("Connection closed\n");
  SetStatusText("Disconnected");
}

void PotbotFrame::OnDisconnected(wxCommandEvent& event) {
  menuFile->Enable(ID_DISCONNECT, false);
  menuFile->Enable(ID_CONNECT, true);
}

void PotbotFrame::SendChatMessage() {
  wxTextCtrl* send_bar_textctrl = (wxTextCtrl*) FindWindowById(ID_SENDBARTEXT);
  std::string message = std::string(send_bar_textctrl->GetValue().mb_str());
  if(message.size() > 0 ) {
    send_bar_textctrl->Clear();
    potbot_->IoService()->post(
        boost::bind(&BnSessionManager::StaticSendChatMessage,
        potbot_->SessionManager(),
        message));
  }
}
