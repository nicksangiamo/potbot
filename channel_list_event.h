#ifndef CHANNEL_LIST_H
#define CHANNEL_LIST_H

#include <wx/wx.h>
#include <list>
#include <string>

class ChannelListEvent : public wxEvent {
 public:
   ChannelListEvent(wxWindow* window = (wxWindow*)NULL,
                    const std::list<std::string>& channel_list = {});
   //ChannelListEvent(const wxString& message, wxWindow* window = (wxWindow*)NULL);
   wxEvent* Clone() const { return new ChannelListEvent(*this); }
   std::list<wxString> ChannelList();
   //wxString Username();
 private:
   //wxString username_;
   std::list<wxString> channel_list_;
   void set_channel_list(const std::list<std::string>& channel_list);

   DECLARE_DYNAMIC_CLASS(ChannelListEvent)
};

typedef void (wxEvtHandler::*ChannelListEventFunction)(ChannelListEvent&);

BEGIN_DECLARE_EVENT_TYPES()
  DECLARE_EVENT_TYPE( myEVT_CHANNELLISTEVENT, 1 )
END_DECLARE_EVENT_TYPES()

#define EVT_CHANNELLISTEVENT(func) DECLARE_EVENT_TABLE_ENTRY( myEVT_CHANNELLISTEVENT, -1, -1, (wxObjectEventFunction)(ChannelListEventFunction) & func, (wxObject*)NULL),

#endif
