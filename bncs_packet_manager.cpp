#include <cassert>
#include <cstring>
#include <iostream>
#include <sstream>
#include <list>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/algorithm/string.hpp>
#include "bn_session_manager.h"
#include "bn_connection.h"
#include "bncs_ids.h"
#include "bncs_packet.h"
#include "xsha1.h"
#include "bncs_packet_manager.h"

using std::string;
using std::stringstream;
using std::list;
using boost::asio::io_service;

constexpr int BncsPacket::kValidIds[];

BncsPacketManager::BncsPacketManager(BnSessionManager* session_manager,
                                     boost::asio::io_service* io_service) {
  set_connection(new BnConnection(BnConnection::kBncsConnection,
                                 "199.108.55.52", "6112", this, io_service));
  set_logger(new Logger("BncsPacketManager"));
  set_io_service(io_service);
  set_session_manager(session_manager);
}

BncsPacketManager::BncsPacketManager(string hostname,
                                     BnSessionManager* session_manager,
                                     boost::asio::io_service* io_service) {
  connection_ = new BnConnection(BnConnection::kBncsConnection, hostname,
                                 "6112", this, io_service);
  logger_ = new Logger("BncsPacketManager");
  set_io_service(io_service);
  set_session_manager(session_manager);
}

bool BncsPacketManager::Init() {
    InitPacketHandlers();
    return BnPacketManager::Init();
}

void BncsPacketManager::Connected() {
    session_manager_->set_bncs_connected(true);
}

void BncsPacketManager::HandlePacket(byte* packet_data,
                                     packet_size_t packet_length) {
  BncsPacket bncs_packet(packet_data, packet_length);
  logger_->Log("handling packet...");
  byte id = bncs_packet.id();
  if(packet_handlers_.count(id)) {
    (session_manager_->*packet_handlers_[id])(bncs_packet);
  } else {
    stringstream ss;
    ss << "Unknown packet id: " << id;
    logger_->Log(ss.str());
  }
}

void BncsPacketManager::SendPacket(BncsPacket& bncs_packet) {
  connection_->SendPacket(bncs_packet);
}

list<RawBnPacket> BncsPacketManager::ExtractRawPackets(byte* data,
                                                       int num_bytes) {
  list<RawBnPacket> raw_packets;
  for(int byte_index=0; byte_index < num_bytes-BncsPacket::kBncsHeaderLen;
      byte_index++) {
    byte ff_byte = data[byte_index];
    byte packet_id = data[byte_index + sizeof(ff_byte)];
    byte* packet_length_ptr = data + byte_index + sizeof(ff_byte) +
                              sizeof(packet_id);
    packet_size_t packet_length = (packet_length_ptr[0] << 0) |
                                  (packet_length_ptr[1] << 8);
    //printf("ff: %02X, id: %02X, len: %04X\n", ff_byte, packet_id, packet_length);
    if(byte_index+packet_length > num_bytes) {
      continue;
    }
    if(ff_byte == 0xff) {
      for(int i=0; i<sizeof(BncsPacket::kValidIds); i++) {
        if(BncsPacket::kValidIds[i] == packet_id) {
          struct RawBnPacket raw_packet = {data+byte_index, packet_length};
          raw_packets.push_back(raw_packet);
          byte_index += packet_length-1;
        }
      }
    }
  }
  return raw_packets;
}

void BncsPacketManager::SendAuthInfo() {
  logger_->Log("Sending SID_AUTH_INFO");
  BncsPacket auth_info_packet(SID_AUTH_INFO);
  //auth_info_packet.set_id(SID_AUTH_INFO); //Packet ID
  auth_info_packet.AddDword(0x00000000); //Protocol ID
  auth_info_packet.AddDword(0x49583836); //Platform ID (IX86)
  auth_info_packet.AddDword(0x53455850); //Product ID (SEXP)
  auth_info_packet.AddDword(0x000000d3); //Version byte (D3)
  auth_info_packet.AddDword(0x00000000); //Product language
  auth_info_packet.AddDword(0x00000000); //Local IP for NAT compatibility
  auth_info_packet.AddDword(0x000000f0); //Time zone bias
  auth_info_packet.AddDword(0x00000409); //Locale ID
  auth_info_packet.AddDword(0x00000409); //Language ID
  auth_info_packet.AddDword(0x00415355); //Country abbreviation (USA)
  auth_info_packet.AddString("United States"); //Country
  //auth_info_packet.Log(logger_);
  SendPacket(auth_info_packet);
}

void BncsPacketManager::SendPing(dword received_ping) {
  logger_->Log("Sending SID_PING");
  BncsPacket ping_packet(SID_PING);
  ping_packet.AddDword(received_ping);
  //ping_packet.Log(logger_);
  SendPacket(ping_packet);
}

void BncsPacketManager::SendAuthCheck(dword client_token,
                                      dword exe_hash,
                                      dword* hashed_key_data,
                                      string exe_info) {
  logger_->Log("Sending SID_AUTH_CHECK");
  BncsPacket auth_check_packet(SID_AUTH_CHECK);
  auth_check_packet.AddDword(client_token); //Client token
  auth_check_packet.AddDword(0x01100101); //Exe version (1.16.1.1)
  auth_check_packet.AddDword(exe_hash); //Exe hash
  auth_check_packet.AddDword(0x00000001); //Number of CD-keys
  auth_check_packet.AddDword(0x00000000); //Using spawn CD-key (false)
  auth_check_packet.AddDword(0x0000000d); //CD-key length (13)
  auth_check_packet.AddDword(0x00000001); //Product value (STAR)
  auth_check_packet.AddDword(0x0059db69); //Public value
  auth_check_packet.AddDword(0x00000000); //Unkown value
  auth_check_packet.AddDwordArray(hashed_key_data, 5); //Hashed key data
  auth_check_packet.AddString(exe_info);
  auth_check_packet.AddString("PoTz"); //CD-key owner
  //auth_check_packet.Log(logger_);
  SendPacket(auth_check_packet);
}

void BncsPacketManager::SendLogonresponse2(dword client_token,
                                           dword server_token,
                                           string& username,
                                           string& password) {
  logger_->Log("Sending SID_LOGONRESPONSE2");
  BncsPacket logonresponse2_packet(SID_LOGONRESPONSE2);
  logonresponse2_packet.AddDword(client_token);
  logonresponse2_packet.AddDword(server_token);
  dword password_hash[5];
  GetPasswordHash(password, client_token, server_token, password_hash);
  logonresponse2_packet.AddDwordArray(password_hash, 5);
  logonresponse2_packet.AddString(username);
  //logonresponse2_packet.Log(logger_);
  SendPacket(logonresponse2_packet);
}

void BncsPacketManager::SendEnterchat(string& username) {
  logger_->Log("Sending SID_ENTERCHAT");
  BncsPacket enterchat_packet(SID_ENTERCHAT);
  enterchat_packet.AddString(username);
  enterchat_packet.AddString("");
  //enterchat_packet.Log(logger_);
  SendPacket(enterchat_packet);
}

void BncsPacketManager::SendGetchannellist(dword product_id) {
  logger_->Log("Sending SID_GETCHANNELLIST");
  BncsPacket getchannellist_packet(SID_GETCHANNELLIST);
  getchannellist_packet.AddDword(product_id);
  //getchannellist_packet.Log(logger_);
  SendPacket(getchannellist_packet);
}

void BncsPacketManager::SendJoinchannel(string& channel, dword flags) {
  logger_->Log("Sending SID_JOINCHANNEL");
  BncsPacket joinchannel_packet(SID_JOINCHANNEL);
  joinchannel_packet.AddDword(flags);
  joinchannel_packet.AddString(channel);
  joinchannel_packet.Log(logger_);
  SendPacket(joinchannel_packet);
}

void BncsPacketManager::SendChatcommand(string& text) {
  logger_->Log("Sending SID_CHATCOMMAND");
  BncsPacket chatcommand_packet(SID_CHATCOMMAND);
  chatcommand_packet.AddString(text);
  chatcommand_packet.Log(logger_);
  SendPacket(chatcommand_packet);
}

void BncsPacketManager::GetPasswordHash(string password,
                                        dword client_token,
                                        dword server_token,
                                        dword* output_hash) {
  boost::to_lower(password);
  dword hash1[5];
  xsha1_calcHashBuf(password.c_str(), password.size(), (dword*)hash1);
  int appended_size = sizeof(hash1) + 2*sizeof(dword);
  byte appended[appended_size + 1];
  memcpy(appended, &client_token, sizeof(dword));
  memcpy(appended+sizeof(dword), &server_token, sizeof(dword));
  memcpy(appended+2*sizeof(dword), hash1, sizeof(hash1));
  appended[appended_size] = 0;
  xsha1_calcHashBuf((const char*)appended, appended_size, (dword*)output_hash);
}

dword BncsPacketManager::ExtractDllDigit(BncsPacket& auth_info_packet) {
  int version_filename_offset = 20;
  int version_filename_length = 21;
  assert(auth_info_packet.id() == SID_AUTH_INFO);
  assert(auth_info_packet.length() >= 
      version_filename_offset + version_filename_length +
      BncsPacket::kBncsHeaderLen);
  string version_filename = auth_info_packet.ExtractString(
      version_filename_offset);
  int dash_index = version_filename.find_last_of("-");
  string dll_digit_str = version_filename.substr(dash_index+1, 2);
  stringstream ss;
  dword dll_digit;
  ss << dll_digit_str;
  ss >> dll_digit;
  return dll_digit;
}

void BncsPacketManager::InitPacketHandlers() {
  packet_handlers_[SID_PING] = &BnSessionManager::HandleSidPing;
  packet_handlers_[SID_AUTH_INFO] = &BnSessionManager::HandleSidAuthInfo;
  packet_handlers_[SID_AUTH_CHECK] = &BnSessionManager::HandleSidAuthCheck;
  packet_handlers_[SID_LOGONRESPONSE2] =
    &BnSessionManager::HandleSidLogonresponse2;
  packet_handlers_[SID_GETCHANNELLIST] =
    &BnSessionManager::HandleSidGetchannellist;
  packet_handlers_[SID_ENTERCHAT] = &BnSessionManager::HandleSidEnterchat;
  packet_handlers_[SID_CHATEVENT] = &BnSessionManager::HandleSidChatevent;
}
