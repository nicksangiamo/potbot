#ifndef BNLS_PACKET_MANAGER_H
#define BNLS_PACKET_MANAGER_H

#include <string>
#include <boost/asio.hpp>
#include "bn_packet_manager.h"
#include "bnls_packet.h"

class BnSessionManager;

class BnlsPacketManager : public BnPacketManager {
 public:
   BnlsPacketManager(BnSessionManager* session_manager,
                     boost::asio::io_service* io_service);
   BnlsPacketManager(std::string hostname,
                     BnSessionManager* session_manager,
                     boost::asio::io_service* io_service);
   bool Init();
   void Connected();

   void HandlePacket(byte* data, packet_size_t length);
   void SendBnlsPacket(BnlsPacket& bnls_packet);

   virtual std::list<RawBnPacket> ExtractRawPackets(byte* data, int num_bytes);

   void SendAuthorize();
   void SendAuthorizeproof();
   void SendRequestversionbyte();
   void SendVersioncheck(dword dll_digit, std::string checksum_formula);
   void SendCdkey(dword sever_token_, std::string cd_key);
   void SendHashdata(std::string password, dword server_token,
                      dword client_token);
 private:
   typedef void (BnSessionManager::*packet_handler)(BnlsPacket& bnls_packet);
   std::map<int, packet_handler> packet_handlers_;

   void InitPacketHandlers();
};

#endif
