#ifndef LOG_H
#define LOG_H

#include <string>

class Logger
{
    private:
        std::string prepend;
        void write_prepend();
    public:
        Logger();
        Logger(std::string);
        void Log(std::string);
        void LogInt(std::string, int);
        void log_string(std::string, std::string);
};

#endif
