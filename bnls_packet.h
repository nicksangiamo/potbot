#ifndef BNLS_PACKET_H
#define BNLS_PACKET_H

#include <vector>
#include <stdint.h>
#include "bnls_ids.h"
#include "bn_packet.h"

class BnlsPacket : public BnPacket {
 public:
   static const packet_size_t kBnlsHeaderLen = 3;
   static constexpr int kValidIds[] = {
       BNLS_NULL                ,
       BNLS_CDKEY               ,
       BNLS_LOGONCHALLENGE      ,
       BNLS_LOGONPROOF          ,
       BNLS_CREATEACCOUNT       ,
       BNLS_CHANGECHALLENGE     ,
       BNLS_CHANGEPROOF         ,
       BNLS_UPGRADECHALLENGE    ,
       BNLS_UPGRADEPROOF        ,
       BNLS_VERSIONCHECK        ,
       BNLS_CONFIRMLOGON        ,
       BNLS_HASHDATA            ,
       BNLS_CDKEY_EX            ,
       BNLS_CHOOSENLSREVISION   ,
       BNLS_AUTHORIZE           ,
       BNLS_AUTHORIZEPROOF      ,
       BNLS_REQUESTVERSIONBYTE  ,
       BNLS_VERIFYSERVER        ,
       BNLS_RESERVESERVERSLOTS  ,
       BNLS_SERVERLOGONCHALLENGE,
       BNLS_SERVERLOGONPROOF    ,
       BNLS_VERSIONCHECKEX      ,
       BNLS_VERSIONCHECKEX2     ,
       BNLS_WARDEN              ,
       BNLS_IPBAN               
   };

   BnlsPacket();
   BnlsPacket(byte);
   BnlsPacket(byte*, packet_size_t);

   byte* ToBuffer();
   packet_size_t GetDataLength();

   static bool IsValidPacket(byte*, int);
};

#endif
