#include <wx/wxprec.h>

#ifndef WX_PRECOMP
  #include <wx/wx.h>
#endif

#include <string>
#include <iostream>
#include <cstdlib>
#include <list>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include <boost/bind.hpp>
#include <boost/property_tree/exceptions.hpp>
#include "bn_session_manager.h"
#include "chat_window_event.h"
#include "potbot_frame.h"
#include "potbot_config.h"
#include "potbot.h"

using namespace std;
using boost::property_tree::ptree_error;

IMPLEMENT_APP(Potbot)

bool Potbot::OnInit() {
  try {
    config_ = new PotbotConfig(config_filename_);
  } catch(ptree_error ptree_error) {
    cerr << ptree_error.what() << endl;
    cerr << "Fatal error: could not load config file \"" << config_filename_
      << "\"" << endl;
    exit(1);
  }
  frame_ = new PotbotFrame(this, "PoTBoT", wxPoint(50, 50), wxSize(450, 340));
  frame_->Show(true);
  return true;
}

void Potbot::Connect() {
  if(in_session_) {
    Disconnect();
  }  
  io_service_ = new boost::asio::io_service();
  session_manager_ = new BnSessionManager(this);
  bn_session_thread_ = new boost::thread(boost::bind(&Potbot::InitConnect,
                                                   this));
  in_session_ = true;
}

void Potbot::Disconnect() {
  delete session_manager_;
  io_service_->post(
      boost::bind(&BnSessionManager::StaticEndSession,
      session_manager_));
  bn_session_thread_->interrupt();
  io_service_->stop();
  delete io_service_;
  in_session_ = false;
}

boost::asio::io_service* Potbot::IoService() {
  return io_service_;
}

PotbotFrame* Potbot::Frame() {
  return frame_;
}

BnSessionManager* Potbot::SessionManager() {
  return session_manager_;
}

void Potbot::InitConnect() {
  session_manager_->Init();
  map<string,string> login_params;
  login_params["Username"] = config_->Get("Login.Username");
  login_params["Password"] = config_->Get("Login.Password");
  login_params["CdKey"] = config_->Get("Login.CdKey");
  login_params["DefaultChannel"] = config_->Get("Login.DefaultChannel");

  for(const pair<string,string>& login_param : login_params) {
    if(login_param.second.compare("") == 0) {
      cerr << "Fatal error: could not read \"" << login_param.first << "\" from "
        << config_filename_ << endl;
      exit(1);
    }
    cout << login_param.first << ": " << login_param.second << endl;
  }
  session_manager_->BeginLogon(login_params["Username"],
                               login_params["Password"], login_params["CdKey"],
                               login_params["DefaultChannel"]);
  io_service_->run();
  try { while(true) { boost::this_thread::interruption_point(); } }
  catch(boost::thread_interrupted& thread_interrupted) {}
}
