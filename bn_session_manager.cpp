#include <string>
#include <sstream>
#include <iostream>
#include <wx/event.h>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/date_time.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>
#include "bncs_packet_manager.h"
#include "bnls_packet_manager.h"
#include "chatevent_ids.h"
#include "logger.h"
#include "chat_window_event.h"
#include "channel_list_event.h"
#include "potbot_frame.h"
#include "bn_session_manager.h"

using std::string;
using std::to_string;
using std::stringstream;
using std::locale;
using boost::asio::deadline_timer;
using boost::system::error_code;
using namespace boost::posix_time;

DEFINE_EVENT_TYPE(myEVT_CONNECTED)
DEFINE_EVENT_TYPE(myEVT_DISCONNECTED)

BnSessionManager::BnSessionManager(Potbot* potbot) {
  potbot_ = potbot;
  io_service_ = potbot->IoService();
  bnls_packet_manager_ = new BnlsPacketManager(this, io_service_);
  bncs_packet_manager_ = new BncsPacketManager(this, io_service_);
  logger_ = new Logger("BnSessionManager");
  bnls_connected_ = false;
  bncs_connected_ = false;
}

BnSessionManager::~BnSessionManager() {
  delete bnls_packet_manager_;
  delete bncs_packet_manager_;
  wxCommandEvent* disconnected_event = new wxCommandEvent(
      myEVT_DISCONNECTED,
      PotbotFrame::ID_DISCONNECTED);
  potbot_->Frame()->GetEventHandler()->QueueEvent(disconnected_event);
}

bool BnSessionManager::Init() {
  InitChatEventHandlers();
  LogChatWindowMessage("Attempting to connect...");
  bnls_packet_manager_->Init();
  bncs_packet_manager_->Init();
  return true;
}

bool BnSessionManager::BeginLogon(const string& username, const string& password) {
  username_ = username;
  password_ = password;
  logger_->Log("Attempting logon");
  WaitBnlsConnected();
  return true;
}

bool BnSessionManager::BeginLogon(const string& username,
                                  const string& password,
                                  const string& cdkey,
                                  const string& default_channel) {
  username_ = username;
  password_ = password;
  cdkey_ = cdkey;
  channel_ = default_channel_ = default_channel;
  BeginLogon(username_, password_);
}

void BnSessionManager::WaitBnlsConnected() {
  if(bnls_connected_) {
    BnlsConnected();
  } else {
    int recheck_interval_ms = 100;
    assert(kServerConnectTimeoutMs % recheck_interval_ms == 0);
    deadline_timer* callback_timer = new deadline_timer(
        *io_service_,
        milliseconds(recheck_interval_ms));
    callback_timer->async_wait(
        boost::bind(&BnSessionManager::WaitBnlsConnectedCallback,
                    this, logger_, recheck_interval_ms,
                    callback_timer, boost::asio::placeholders::error));
  }
}

void BnSessionManager::WaitBncsConnected() {
  if(bncs_connected_) {
    BncsConnected();
  } else {
    int recheck_interval_ms = 100;
    assert(kServerConnectTimeoutMs % recheck_interval_ms == 0);
    deadline_timer* callback_timer = new deadline_timer(
        *io_service_,
        milliseconds(recheck_interval_ms));
    callback_timer->async_wait(
        boost::bind(&BnSessionManager::WaitBncsConnectedCallback,
                    this, logger_, recheck_interval_ms,
                    callback_timer, boost::asio::placeholders::error));
  }
}

void BnSessionManager::WaitBnlsConnectedCallback(
    BnSessionManager* session_manager,
    Logger* logger,
    int recheck_interval_ms,
    deadline_timer* callback_timer,
    const error_code& timer_error) {
  assert(kServerConnectTimeoutMs % recheck_interval_ms == 0);
  static int ms_waited = 0;
  if(!timer_error) {
    ms_waited += recheck_interval_ms;
    if(session_manager->bnls_connected_) {
      delete callback_timer;
      session_manager->BnlsConnected();
    } else {
      if(ms_waited >= kServerConnectTimeoutMs) {
        delete callback_timer;
        session_manager->BnlsConnectTimeout();
      } else {
        callback_timer->expires_from_now(
            milliseconds(recheck_interval_ms));
        callback_timer->async_wait(
            boost::bind(&BnSessionManager::WaitBnlsConnectedCallback,
                        session_manager, logger, recheck_interval_ms,
                        callback_timer, boost::asio::placeholders::error));
      }
    }
  } else {
    logger->Log("timer error: " + timer_error.message());
  }
}

void BnSessionManager::WaitBncsConnectedCallback(
    BnSessionManager* session_manager,
    Logger* logger,
    int recheck_interval_ms,
    deadline_timer* callback_timer,
    const error_code& timer_error) {
  assert(kServerConnectTimeoutMs % recheck_interval_ms == 0);
  static int ms_waited = 0;
  if(!timer_error) {
    ms_waited += recheck_interval_ms;
    if(session_manager->bncs_connected_) {
      delete callback_timer;
      session_manager->BncsConnected();
    } else {
      if(ms_waited >= kServerConnectTimeoutMs) {
        delete callback_timer;
        session_manager->BncsConnectTimeout();
      } else {
        callback_timer->expires_from_now(
            milliseconds(recheck_interval_ms));
        callback_timer->async_wait(
            boost::bind(&BnSessionManager::WaitBncsConnectedCallback,
                        session_manager, logger, recheck_interval_ms,
                        callback_timer, boost::asio::placeholders::error));
      }
    }
  } else {
    logger->Log("Timer error: " + timer_error.message());
  }
}

void BnSessionManager::StaticSendChatMessage(BnSessionManager* session_manager,
                                             string message) {
  puts("STATIC SEND CHAT");
  puts(message.c_str());
  session_manager->SendChatMessage(message);
}

void BnSessionManager::InitChatEventHandlers() {
  chat_event_handlers_[EID_SHOWUSER] = &BnSessionManager::HandleEidShowuser;
  chat_event_handlers_[EID_JOIN] = &BnSessionManager::HandleEidJoin;
  chat_event_handlers_[EID_LEAVE] = &BnSessionManager::HandleEidLeave;
  chat_event_handlers_[EID_WHISPER] = &BnSessionManager::HandleEidWhisper;
  chat_event_handlers_[EID_TALK] = &BnSessionManager::HandleEidTalk;
  chat_event_handlers_[EID_BROADCAST] = &BnSessionManager::HandleEidBroadcast;
  chat_event_handlers_[EID_CHANNEL] = &BnSessionManager::HandleEidChannel;
  chat_event_handlers_[EID_USERFLAGS] = &BnSessionManager::HandleEidUserflags;
  chat_event_handlers_[EID_WHISPERSENT] =
    &BnSessionManager::HandleEidWhispersent;
  chat_event_handlers_[EID_CHANNELFULL] =
    &BnSessionManager::HandleEidChannelfull;
  chat_event_handlers_[EID_CHANNELDOESNOTEXIST] = &BnSessionManager::HandleEidChanneldoesnotexist;
  chat_event_handlers_[EID_CHANNELRESTRICTED] = &BnSessionManager::HandleEidChannelrestricted;
  chat_event_handlers_[EID_INFO] = &BnSessionManager::HandleEidInfo;
  chat_event_handlers_[EID_ERROR] = &BnSessionManager::HandleEidError;
  chat_event_handlers_[EID_IGNORE] = &BnSessionManager::HandleEidIgnore;
  chat_event_handlers_[EID_ACCEPT] = &BnSessionManager::HandleEidAccept;
  chat_event_handlers_[EID_EMOTE] = &BnSessionManager::HandleEidEmote;
}

void BnSessionManager::StaticEndSession(BnSessionManager* session_manager) {
  session_manager->EndSession();
}

void BnSessionManager::BnlsConnected() {
  logger_->Log("BNLS connection confirmed, beginning logon");
  LogChatWindowMessage("Initiating BNLS logon sequence");
  LogChatWindowMessage("Attempting BNLS authorization...");
  bnls_packet_manager_->SendAuthorize();
}

void BnSessionManager::BnlsConnectTimeout() {
  logger_->Log("Logon attempt timed out waiting for connection to bnls server");
  LogChatWindowMessage(
      "Connect attempt timed out waiting for BNLS connection");
}

void BnSessionManager::BncsConnected() {
  logger_->Log("BNCS connection confirmed, continuing with logon");
  wxCommandEvent* connected_event = new wxCommandEvent(
      myEVT_CONNECTED,
      PotbotFrame::ID_CONNECTED);
  potbot_->Frame()->GetEventHandler()->QueueEvent(connected_event);
  //UpdateStatusBar("Connected");
  LogChatWindowMessage("Initiating BNCS logon sequence");
  bncs_packet_manager_->SendGameProtocolByte();
  bnls_packet_manager_->SendRequestversionbyte();
}

void BnSessionManager::BncsConnectTimeout() {
  logger_->Log("Logon attempt timed out waiting for connection to bncs server");
  LogChatWindowMessage(
      "Logon attempt timed out waiting for BNCS connection");
}

void BnSessionManager::EndSession() {
  io_service_->stop();
}

void BnSessionManager::SendChatMessage(string& message) {
  if(!LoggedOn()) {
    return;
  }
  string to_send = ValidateChatMessage(message);
  bncs_packet_manager_->SendChatcommand(to_send);
  if(message[0] != '/') {
    LogChatWindowMessage(username_ + ": " + to_send);
  }
}

bool BnSessionManager::LoggedOn() {
  return logged_on_;
}

string BnSessionManager::ValidateChatMessage(string& message) {
  string validated_message = "";
  int char_counter = 0;
  for(char& c : message) {
    if(c < 0x20) {
      continue;
    }
    if(char_counter > 223) {
      break;
    }
    validated_message += c;
    char_counter++;
  }
  return validated_message;
}

void BnSessionManager::set_bnls_connected(bool connected) {
  logger_->Log("BNLS connected!");
  LogChatWindowMessage("Established BNLS connection!");
  bnls_connected_ = connected;
}

void BnSessionManager::set_bncs_connected(bool connected) {
  logger_->Log("BNCS connected!");
  LogChatWindowMessage("Established BNCS connection!");
  bncs_connected_ = connected;
}

bool BnSessionManager::LogChatWindowMessage(const string& message) {
  ptime now = second_clock::local_time();
  time_facet* facet = new time_facet("(%I:%M:%S %p)");
  stringstream ss;
  ss.imbue(locale(ss.getloc(), facet));
  ss << now << " " << message;
  string to_log = ss.str();
  ChatWindowEvent* chat_window_event = new ChatWindowEvent(potbot_->Frame(), to_log);
  potbot_->Frame()->GetEventHandler()->QueueEvent(chat_window_event);
  //TODO: MAKE SURE EVENT GETS FREED
  return true;
}

void BnSessionManager::UpdateStatusBar(const string& status) {
  StatusBarEvent* status_bar_event = new StatusBarEvent(potbot_->Frame(),
                                                        status);
  potbot_->Frame()->GetEventHandler()->QueueEvent(status_bar_event);
}

void BnSessionManager::HandleBnlsAuthorize(BnlsPacket& authorize_packet) {
  logger_->Log("Got BNLS_AUTHORIZE");
  bnls_packet_manager_->SendAuthorizeproof();
}

void BnSessionManager::HandleBnlsAuthorizeproof(
    BnlsPacket& authorizeproof_packet) {
  logger_->Log("Got BNLS_AUTHORIZEPROOF");
  dword auth_status = authorizeproof_packet.ExtractDword(0);
  if(auth_status == 0) {
    LogChatWindowMessage("BNLS authorization success!");
    WaitBncsConnected();
  } else {
    logger_->Log("BNLS authorization failure");
    LogChatWindowMessage("BNLS authorization failure");
    //TODO: Some sort of error handling function?
  }
}

void BnSessionManager::HandleBnlsVersioncheck(
    BnlsPacket& versioncheck_packet) {
  logger_->Log("Got BNLS_VERSIONCHECK");
  dword versioncheck_status = versioncheck_packet.ExtractDword(0);
  if(versioncheck_status != 0) {
    logger_->Log("VERSIONCHECK success");
    LogChatWindowMessage("BNLS VERSIONCHECK success!");
    exe_hash_ = versioncheck_packet.ExtractDword(8);
    exe_info_ = versioncheck_packet.ExtractString(12);
    LogChatWindowMessage("Attempting BNLS CDKEY...");
    bnls_packet_manager_->SendCdkey(server_token_, cdkey_);
  } else {
    logger_->Log("BNLS VERSIONCHECK failed");
    LogChatWindowMessage("BNLS VERSIONCHECK failure");
  }
}

void BnSessionManager::HandleBnlsRequestversionbyte(
    BnlsPacket& requestversionbyte_packet) {
  logger_->Log("Got BNLS_REQUESTVERSIONBYTE");
  bncs_packet_manager_->SendAuthInfo();
}

void BnSessionManager::HandleBnlsCdkey(BnlsPacket& cdkey_packet) {
  logger_->Log("Got BNLS_CDKEY");
  dword result = cdkey_packet.ExtractDword(0);
  if(result) {
    LogChatWindowMessage("BNLS CDKEY success!");
    logger_->Log("BNLS_CDKEY success!");
    client_token_ = cdkey_packet.ExtractDword(4);
    cdkey_packet.ExtractDwordArray(24, 5, hashed_key_data_);
    LogChatWindowMessage("Attempting BNCS AUTH_CHECK...");
    bncs_packet_manager_->SendAuthCheck(client_token_, exe_hash_,
                                        hashed_key_data_, exe_info_);
  } else {
    LogChatWindowMessage("BNLS CDKEY failure");
    logger_->Log("BNLS_CDKEY failure!");
  }
}

void BnSessionManager::HandleBnlsHashdata(BnlsPacket& hashdata_packet) {
  logger_->Log("Got BNLS_HASHDATA");
  dword hash_data[5];
  hashdata_packet.ExtractDwordArray(0, 5, hash_data);
}

void BnSessionManager::HandleSidPing(BncsPacket& ping_packet) {
  logger_->Log("Got SID_PING");
  received_ping_ = ping_packet.ExtractDword(0);
  bncs_packet_manager_->SendPing(received_ping_);
}

void BnSessionManager::HandleSidAuthInfo(BncsPacket& auth_info_packet) {
  logger_->Log("Got SID_AUTH_INFO");
  server_token_ = auth_info_packet.ExtractDword(4);
  dll_digit_ = bncs_packet_manager_->ExtractDllDigit(auth_info_packet); 
  checksum_formula_ = auth_info_packet.ExtractString(41);
  LogChatWindowMessage("Attempting BNLS VERSIONCHECK...");
  bnls_packet_manager_->SendVersioncheck(dll_digit_, checksum_formula_);
}

void BnSessionManager::HandleSidAuthCheck(BncsPacket& auth_check_packet) {
  logger_->Log("Got SID_AUTH_CHECK");
  dword result = auth_check_packet.ExtractByte(0);
  switch(result) {
    case 0x000: {
      LogChatWindowMessage("BNCS AUTH_CHECK success!");
      logger_->Log("SID_AUTH_CHECK PASS!");
      LogChatWindowMessage("Attempting logon...");
      bncs_packet_manager_->SendLogonresponse2(client_token_, server_token_,
                                                username_, password_);
      break;
    }
    case 0x100:
      logger_->Log("SID_AUTH_CHECK FAIL: Old game version");
      LogChatWindowMessage("BNCS AUTH_CHECK failure: old game version");
      break;
    case 0x101:
      logger_->Log("SID_AUTH_CHECK FAIL: Invalid version");
      LogChatWindowMessage("BNCS AUTH_CHECK failure: invalid version");
      break;
    case 0x102:
        logger_->Log("SID_AUTH_CHECK FAIL: Game version must be downgraded");
        LogChatWindowMessage(
            "BNCS AUTH_CHECK failure: game version must be downgraded");
        break;
    case 0x200:
        logger_->Log("SID_AUTH_CHECK FAIL: invalid CD key");
        LogChatWindowMessage("BNCS AUTH_CHECK failure: invalid CD key");
        break;
    case 0x201:
        logger_->Log("SID_AUTH_CHECK FAIL: CD key in use");
        LogChatWindowMessage("BNCS AUTH_CHECK failure: CD key already in use");
        //TODO: get name of user
        break;
    case 0x202:
        logger_->Log("SID_AUTH_CHECK FAIL: banned CD key");
        LogChatWindowMessage("BNCS AUTH_CHECK failure: CD key is banned");
        break;
    case 0x203:
        logger_->Log("SID_AUTH_CHECK FAIL: wrong product");
        LogChatWindowMessage("BNCS AUTH_CHECK failure: Wrong product");
        break;
    default:
        logger_->Log("SID_AUTH_CHECK FAIL: returned status code was " +
                     to_string(result));
        LogChatWindowMessage(
            "BNCS AUTH_CHECK failure with unrecognized error code: " +
            to_string(result));
        //TODO: check for invalid version code
        break;
  }
}

void BnSessionManager::HandleSidLogonresponse2(
    BncsPacket& logonresponse2_packet) {
  logger_->Log("Got SID_LOGONRESPONSE2");
  byte result = logonresponse2_packet.ExtractByte(0);
  switch(result) {
    case 0x00:
      logger_->Log("Logon success!");
      UpdateStatusBar("Logged on as " + username_);
      logged_on_ = true;
      LogChatWindowMessage("Logon success!");
      LogChatWindowMessage("Entering chat...");
      bncs_packet_manager_->SendEnterchat(username_);
      bncs_packet_manager_->SendGetchannellist(0x50584553); //product id
      bncs_packet_manager_->SendJoinchannel(channel_, BncsPacketManager::kForcedJoin);
      break;
    case 0x01:
      logger_->Log("Logon fail: Account does not exist");
      LogChatWindowMessage("Logon failure: account does not exist");
      break;
    case 0x02:
      logger_->Log("Logon fail: Invalid password");
      LogChatWindowMessage("Logon failure: invalid password");
      break;
    case 0x06: {
      string additional_info = logonresponse2_packet.ExtractString(4);
      logger_->Log("Logon fail: Account closed: " + additional_info);
      LogChatWindowMessage("Logon failure: account has been closed");
      break;
    }
    default:
      logger_->Log("Logon failed with unrecognized error code: " +
                   to_string(result));
      LogChatWindowMessage("Logon failure with unregocnigzed status code: " + 
                           to_string(result));
      break;
  }
}

void BnSessionManager::HandleSidGetchannellist(
    BncsPacket& getchannellist_packet) {
  logger_->Log("Got SID_GETCHANNELLIST");
  bn_default_channels_.clear();
  BnPacketIterator packet_iterator = getchannellist_packet.GetIterator(); 
  while(packet_iterator.HasAnotherString()) {
    string channel = packet_iterator.GetNextString();
    if(channel != "") { // last channel is the empty string
      bn_default_channels_.push_back(channel);
    }
  }
}

void BnSessionManager::HandleSidEnterchat(BncsPacket& enterchat_packet) {
  logger_->Log("Got SID_ENTERCHAT");
}

void BnSessionManager::HandleSidChatevent(BncsPacket& chatevent_packet) {
  logger_->Log("Got SID_CHATEVENT");
  BnPacketIterator packet_iterator = chatevent_packet.GetIterator();
  dword event_id = packet_iterator.GetNextDword(); 
  dword flags = packet_iterator.GetNextDword(); 
  dword ping = packet_iterator.GetNextDword();
  dword ip_address = packet_iterator.GetNextDword(); //Defunct
  dword account_number = packet_iterator.GetNextDword(); //Defunct
  dword registration_authority = packet_iterator.GetNextDword(); //Defunct
  string username = packet_iterator.GetNextString();
  string text = packet_iterator.GetNextString();

  (this->*chat_event_handlers_[event_id])(flags, ping, username, text);
}

void BnSessionManager::HandleEidShowuser(dword flags, dword ping,
                                         string username,
                                         string text) {
  logger_->Log("Got EID_SHOWUSER");
  logger_->LogInt("flags", flags);
  logger_->LogInt("ping", ping);
  logger_->log_string("username", username);
  logger_->log_string("text", text);
  channel_list_.push_back(username);
  ChannelListEvent* channel_list_event =
    new ChannelListEvent(potbot_->Frame(), channel_list_);
  potbot_->Frame()->GetEventHandler()->QueueEvent(channel_list_event);
  //TODO: MAKE SURE EVENT GETS FREED
}

void BnSessionManager::HandleEidJoin(dword flags, dword ping,
                                     string username,
                                     string text) {
  logger_->Log("Got EID_JOIN");
  logger_->LogInt("flags", flags);
  logger_->LogInt("ping", ping);
  logger_->log_string("username", username);
  logger_->log_string("text", text);
  channel_list_.push_back(username);
  ChannelListEvent* channel_list_event =
    new ChannelListEvent(potbot_->Frame(), channel_list_);
  potbot_->Frame()->GetEventHandler()->QueueEvent(channel_list_event);
  //TODO: MAKE SURE EVENT GETS FREED
  LogChatWindowMessage(username + " has joined the channel");
}

void BnSessionManager::HandleEidLeave(dword flags, dword ping,
                                      string username,
                                      string text) {
  logger_->Log("Got EID_LEAVE");
  logger_->LogInt("flags", flags);
  logger_->LogInt("ping", ping);
  logger_->log_string("username", username);
  logger_->log_string("text", text);
  channel_list_.remove(username);
  ChannelListEvent* channel_list_event =
    new ChannelListEvent(potbot_->Frame(), channel_list_);
  potbot_->Frame()->GetEventHandler()->QueueEvent(channel_list_event);
  //TODO: MAKE SURE EVENT GETS FREED
  LogChatWindowMessage(username + " has left the channel");
}

void BnSessionManager::HandleEidWhisper(dword flags, dword ping,
                                        string username,
                                        string text) {
  logger_->Log("Got EID_WHISPER");
  logger_->LogInt("flags", flags);
  logger_->LogInt("ping", ping);
  logger_->log_string("username", username);
  logger_->log_string("text", text);
  LogChatWindowMessage("Whisper from " + username + ": " + text);
}

void BnSessionManager::HandleEidTalk(dword flags, dword ping,
                                     string username,
                                     string text) {
  logger_->Log("Got EID_TALK");
  logger_->LogInt("flags", flags);
  logger_->LogInt("ping", ping);
  logger_->log_string("username", username);
  logger_->log_string("text", text);
  LogChatWindowMessage(username  + ": " + text);
}

void BnSessionManager::HandleEidBroadcast(dword flags, dword ping,
                                          string username,
                                          string text) {
  logger_->Log("Got EID_BROADCAST");
  logger_->LogInt("flags", flags);
  logger_->LogInt("ping", ping);
  logger_->log_string("username", username);
  logger_->log_string("text", username);
  LogChatWindowMessage("Broadcast from " + username + ": " +  text);
}

void BnSessionManager::HandleEidChannel(dword flags, dword ping,
                                        string username,
                                        string text) {
  logger_->Log("Got EID_CHANNEL");
  logger_->LogInt("flags", flags);
  logger_->LogInt("ping", ping);
  logger_->log_string("username", username);
  logger_->log_string("text", text);
  channel_list_.clear();
  ChannelListEvent* channel_list_event =
    new ChannelListEvent(potbot_->Frame(), channel_list_);
  potbot_->Frame()->GetEventHandler()->QueueEvent(channel_list_event);
  LogChatWindowMessage("Joined channel " + text);
  UpdateStatusBar("In channel " + text);
}

void BnSessionManager::HandleEidUserflags(dword flags, dword ping,
                                          string username,
                                          string text) {
  logger_->Log("Got EID_USERFLAGS");
  logger_->LogInt("flags", flags);
  logger_->LogInt("ping", ping);
  logger_->log_string("username", username);
  logger_->log_string("text", text);
}

void BnSessionManager::HandleEidWhispersent(dword flags, dword ping,
                                            string username,
                                            string text) {
  logger_->Log("Got EID_WHISPERSENT");
  logger_->LogInt("flags", flags);
  logger_->LogInt("ping", ping);
  logger_->log_string("username", username);
  logger_->log_string("text", text);
  LogChatWindowMessage("Whisper to " + username + ": " + text);
}

void BnSessionManager::HandleEidChannelfull(dword flags, dword ping,
                                            string username,
                                            string text) {
  logger_->Log("Got EID_CHANNELFULL");
  logger_->LogInt("flags", flags);
  logger_->LogInt("ping", ping);
  logger_->log_string("username", username);
  logger_->log_string("text", text);
  LogChatWindowMessage("Channel is full: " + text);
}

void BnSessionManager::HandleEidChanneldoesnotexist(dword flags, dword ping,
                                                    string username,
                                                    string text) {
  logger_->Log("Got EID_CHANNELDOESNOTEXIST");
  logger_->LogInt("flags", flags);
  logger_->LogInt("ping", ping);
  logger_->log_string("username", username);
  logger_->log_string("text", text);
  LogChatWindowMessage("Channel does not exist: " + text);
}

void BnSessionManager::HandleEidChannelrestricted(dword flags, dword ping,
                                                  string username,
                                                  string text) {
  logger_->Log("Got EID_CHANNELRESTRICTED");
  logger_->LogInt("flags", flags);
  logger_->LogInt("ping", ping);
  logger_->log_string("username", username);
  logger_->log_string("text", text);
  LogChatWindowMessage("Channel is restricted: " + text);
}

void BnSessionManager::HandleEidInfo(dword flags, dword ping,
                                     string username,
                                     string text) {
  logger_->Log("Got EID_INFO");
  logger_->LogInt("flags", flags);
  logger_->LogInt("ping", ping);
  logger_->log_string("username", username);
  logger_->log_string("text", text);
  LogChatWindowMessage("Info: " + text);
}

void BnSessionManager::HandleEidError(dword flags, dword ping,
                                      string username,
                                      string text) {
  logger_->Log("Got EID_ERROR");
  logger_->LogInt("flags", flags);
  logger_->LogInt("ping", ping);
  logger_->log_string("username", username);
  logger_->log_string("text", text);
  LogChatWindowMessage("Error: " + text);
}

void BnSessionManager::HandleEidIgnore(dword flags, dword ping,
                                       string username,
                                       string text) {
  logger_->Log("Got EID_IGNORE");
  logger_->LogInt("flags", flags);
  logger_->LogInt("ping", ping);
  logger_->log_string("username", username);
  logger_->log_string("text", text);
  LogChatWindowMessage("Ignoring messages from " + username + ": " + text);
}

void BnSessionManager::HandleEidAccept(dword flags, dword ping,
                                       string username,
                                       string text) {
  logger_->Log("Got EID_ACCEPT");
  logger_->LogInt("flags", flags);
  logger_->LogInt("ping", ping);
  logger_->log_string("username", username);
  logger_->log_string("text", text);
  LogChatWindowMessage("Accepting messages from " + username + ": " +
                       text);
}

void BnSessionManager::HandleEidEmote(dword flags, dword ping,
                                      string username,
                                      string text) {
  logger_->Log("Got EID_EMOTE");
  logger_->LogInt("flags", flags);
  logger_->LogInt("ping", ping);
  logger_->log_string("username", username);
  logger_->log_string("text", text);
  LogChatWindowMessage(username  + " " + text);
}
