#ifndef BNCS_PACKET_MANAGER_H
#define BNCS_PACKET_MANAGER_H

#include "bn_packet_manager.h"
#include <string>
#include <map>
#include <boost/asio.hpp>
#include "bncs_packet.h"

class BnSessionManager;

class BncsPacketManager : public BnPacketManager {
 public:
   const static dword kNoCreateJoin = 0x00;
   const static dword kFirstJoin = 0x01;
   const static dword kForcedJoin = 0x02;
   const static dword kD2FirstJoin = 0x05;
   BncsPacketManager(BnSessionManager* session_manager,
                     boost::asio::io_service* io_service);
   BncsPacketManager(std::string ip_address, BnSessionManager* session_manager,
                     boost::asio::io_service* io_service);
   bool Init();
   void Connected();

   void HandlePacket(byte* packet_data, packet_size_t packet_length);
   void SendPacket(BncsPacket& bncs_packet);

   virtual std::list<RawBnPacket> ExtractRawPackets(byte* data, int num_bytes);

   void SendAuthInfo();
   void SendAuthCheck(dword client_token, dword exe_hash,
                        dword* hashed_key_data, std::string exe_info);
   void SendPing(dword received_ping);
   void SendLogonresponse2(dword client_token, dword server_token,
                            std::string& username, std::string& password);
   void SendEnterchat(std::string& username);
   void SendGetchannellist(dword product_id);
   void SendJoinchannel(std::string& channel, dword flags);
   void SendChatcommand(std::string& text);
 
   dword ExtractDllDigit(BncsPacket& auth_info_packet);
   void GetPasswordHash(std::string password, dword client_token,
                          dword server_token, dword* output_hash);
 private:
   typedef void (BnSessionManager::*packet_handler)(BncsPacket& bncs_packet);
   std::map<int, packet_handler> packet_handlers_;

   void InitPacketHandlers();
};

#endif
