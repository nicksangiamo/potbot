#ifndef CHAT_EVENT_IDS_H
#define CHAT_EVENT_IDS_H

#define  EID_SHOWUSER            0x01
#define  EID_JOIN                0x02
#define  EID_LEAVE               0x03
#define  EID_WHISPER             0x04
#define  EID_TALK                0x05
#define  EID_BROADCAST           0x06
#define  EID_CHANNEL             0x07
#define  EID_USERFLAGS           0x09
#define  EID_WHISPERSENT         0x0A
#define  EID_CHANNELFULL         0x0D
#define  EID_CHANNELDOESNOTEXIST 0x0E
#define  EID_CHANNELRESTRICTED   0x0F
#define  EID_INFO                0x12
#define  EID_ERROR               0x13
#define  EID_IGNORE              0x15
#define  EID_ACCEPT              0x16
#define  EID_EMOTE               0x17

#endif
