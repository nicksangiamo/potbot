#ifndef POTBOT_FRAME_H
#define POTBOT_FRAME_H

#include "wx/wxprec.h"
#ifndef WX_PRECOMP
    #include "wx/wx.h"
#endif
#include "potbot.h"
#include "chat_window_event.h"
#include "status_bar_event.h"
#include "channel_list_event.h"
#include <string>

class PotbotFrame: public wxFrame {
 friend class BnSessionManager;
 public:
   PotbotFrame(Potbot* potbot, const wxString& title, const wxPoint& pos,
               const wxSize& size);
   void DisplayMessage(const std::string& message);
 private:
   enum {
     ID_CONNECT = 1,
     ID_CONNECTED = 2,
     ID_DISCONNECT = 3,
     ID_DISCONNECTED = 4,
     ID_CHATWINDOW = 5,
     ID_CHANNELLIST = 6,
     ID_SENDBUTTON = 7,
     ID_SENDBARTEXT = 8
   };
   Potbot* potbot_;
   wxMenu* menuFile;
   void OnChatWindowEvent(ChatWindowEvent& chat_window_event);
   void OnStatusBarEvent(StatusBarEvent& status_bar_event);
   void OnChannelListEvent(ChannelListEvent& channel_list_event);
   void OnSendButtonEvent(wxCommandEvent& event);
   void OnKey(wxKeyEvent& event);
   void OnConnect(wxCommandEvent& event);
   void OnConnected(wxCommandEvent& event);
   void OnDisconnect(wxCommandEvent& event);
   void OnDisconnected(wxCommandEvent& event);
   void OnExit(wxCommandEvent& event);
   void OnAbout(wxCommandEvent& event);
   void SendChatMessage();
   wxDECLARE_EVENT_TABLE();
};

#endif
