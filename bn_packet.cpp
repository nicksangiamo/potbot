#include "bn_packet.h"
#include <cassert>
#include <iostream>
#include <string>
#include "logger.h"

using std::vector;
using std::cout;
using std::endl;
using std::string;
using std::to_string;

bool BnPacket::IsValidPacket(byte* data, packet_size_t length) {
    return true;
}

BnPacket::BnPacket() : length_(0) {}

BnPacket::BnPacket(byte id) : id_(id), length_(0) {}

BnPacket::BnPacket(byte* data, packet_size_t length)
    : length_(length) {}

BnPacket::BnPacket(BnPacketTypes type, byte id, packet_size_t length)
    : type_(type), id_(id), length_(length) {}

void BnPacket::set_length(byte* length_pointer) {
  length_ = *((packet_size_t*)length_pointer);
}

void BnPacket::set_length(packet_size_t length) {
  length_ = length;
}

void BnPacket::set_type(BnPacketTypes type) {
  type_ = type;
}

packet_size_t BnPacket::length() const {
  return length_;
}

void BnPacket::set_id(byte* id_pointer) {
  id_ = *id_pointer;
}

void BnPacket::set_id(byte id) {
  id_ = id;
}

byte BnPacket::id() const {
  return id_;
}

void BnPacket::set_data(const std::vector<byte>& data) {
  data_ = data;
}

const std::vector<byte> BnPacket::data() const {
  return data_;
}

byte* BnPacket::data_pointer() {
  return data_.data();
}

byte BnPacket::ExtractByte(packet_size_t position) {
  return data_[position];
}

dword BnPacket::ExtractDword(packet_size_t position) {
  assert(position <= length_ - sizeof(dword));
  dword* extracted_dword_pointer = reinterpret_cast<dword*>(data_pointer() + position);
  return *extracted_dword_pointer;
}

dword* BnPacket::ExtractDwordArray(packet_size_t position, packet_size_t num_dwords,
                                        dword* dword_data) {
  assert(position + num_dwords*sizeof(dword) < length_);
  for(int i=0; i<num_dwords; i++) {
    dword_data[i] = ExtractDword(position+i*sizeof(dword));
  }
  return dword_data;
}

string BnPacket::ExtractString(packet_size_t position) {
  string extracted_string;
  for(int i=0; i<length_; i++) {
    char next_char = static_cast<char>(ExtractByte(position+i));
    if(next_char == '\0') {
      break;
    }
    extracted_string += next_char;
  }
  return extracted_string;
}

void BnPacket::Log(Logger* logger) {
  switch(type_) {
    case kBnlsPacket: logger->Log("Logging a BNLS Packet..."); break;
    case kBncsPacket: logger->Log("Logging a BNCS Packet..."); break;
    default:
      logger->Log("Log Error: Unknown packet type: " + to_string(type_));
      return;
  }

  char id_string[5], length_string[4], byte_string[3];
  string id_log_string = "Packet ID: ",
  length_log_string = "Packet length: ",
  data_log_string = "Packet data: ";

  snprintf(id_string, sizeof(id_string), "0x%02X", id());
  snprintf(length_string, sizeof(length_string), "%03d", length());
  id_log_string.append(id_string, sizeof(id_string)-1);
  length_log_string.append(length_string, sizeof(length_string)-1);

  for(auto iter = data_.begin(); iter != data_.end(); iter++) {
    if(iter != data_.begin()) {
      data_log_string.append(" ");
    }
    snprintf(byte_string, 3, "%02X", *iter);
    data_log_string.append(byte_string, 2);
  }

  logger->Log(id_log_string);
  logger->Log(length_log_string);
  logger->Log(data_log_string);
}

void BnPacket::AddByte(byte byte) {
  data_.push_back(byte);
  length_ += sizeof(byte);
}

void BnPacket::AddLittleEndianDword(dword dword) {
  for(int bit_index = 0; bit_index <= 24; bit_index += 8) {
    AddByte(dword & 0xFF);
    dword = dword >> 8;
  }
}

void BnPacket::AddBigEndianDword(dword dword) {
  for(int bit_index = 24; bit_index >= 0; bit_index -= 8) {
    AddByte((dword >> bit_index) & 0xFF);
  }
}

void BnPacket::AddDword(dword dword) {
  AddLittleEndianDword(dword); //so we are using bnetp ordering as the default
}

void BnPacket::AddDwordArray(dword* dword_data, int num_dwords) {
  for(int i=0; i<num_dwords; i++) {
    AddDword(dword_data[i]);
  }
}

void BnPacket::AddString(string the_string) {
  for(char& byte : the_string) {
      AddByte(byte);
  }
  AddByte(0);
}

void BnPacket::LogData(byte* data, packet_size_t data_size, Logger* logger) {
  string data_string;
  for(int data_index=0; data_index!=data_size; data_index++) {
    char byte_buf[3];
    snprintf(byte_buf, 3, "%02X", data[data_index]);
    data_string.append(byte_buf, 2);
    if(data_index != data_size) {
      data_string.append(" ");
    }
  }
  logger->Log(data_string);
}

BnPacketIterator BnPacket::GetIterator() {
  return BnPacketIterator(this);
}

BnPacketIterator::BnPacketIterator() : byte_index_(0) {}

BnPacketIterator::BnPacketIterator(BnPacket* packet)
    : byte_index_(0), packet_(packet) {}

byte BnPacketIterator::GetNextByte() {
  assert(packet_->length() - sizeof(byte) >= byte_index_);
  byte next_byte = packet_->ExtractByte(byte_index_);
  byte_index_ += sizeof(byte);
  return next_byte;
}

dword BnPacketIterator::GetNextDword() {
  assert(packet_->length() - sizeof(dword) >= byte_index_);
  dword next_dword = packet_->ExtractDword(byte_index_);
  byte_index_ += sizeof(dword);
  return next_dword;
}

//Note: Returns empty string if there are no more strings in the packet
//The RemainingStrings method can be used beforehand
string BnPacketIterator::GetNextString() {
  string next_string;
  packet_size_t data_length = packet_->GetDataLength();
  while(byte_index_ < data_length) {
    char next_char = static_cast<char>(packet_->ExtractByte(byte_index_));
    byte_index_++;
    if(next_char == '\0') {
      return next_string;
    }
    next_string += next_char;
  }
  return "";
}

bool BnPacketIterator::HasAnotherString() {
  packet_size_t temp_byte_index = byte_index_;
  packet_size_t data_length = packet_->GetDataLength();
  while(temp_byte_index < data_length) {
    char next_char = static_cast<char>(packet_->ExtractByte(temp_byte_index));
    if(next_char == '\0') {
      return true;
    }
    ++temp_byte_index;
  }
  return false;
}

int BnPacketIterator::RemainingStrings() {
  int remaining_strings = 0;
  int temp_byte_index = byte_index_;
  int packet_length = packet_->length();
  while(temp_byte_index < packet_length) {
    char next_char = static_cast<char>(packet_->ExtractByte(temp_byte_index));
    if(next_char == '\0') {
      ++remaining_strings;
    }
    ++temp_byte_index;
  }
  return remaining_strings;
}

