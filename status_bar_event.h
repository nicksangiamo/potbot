#ifndef STATUS_BAR_EVENT_H
#define STATUS_BAR_EVENT_H

#include <wx/wx.h>

class StatusBarEvent : public wxEvent {
 public:
   StatusBarEvent(wxWindow* window = (wxWindow*)NULL,
               const wxString& status = "");
   wxEvent* Clone() const { return new StatusBarEvent(*this); }
   wxString Status();
 private:
   wxString status_;

   DECLARE_DYNAMIC_CLASS(StatusBarEvent)
};

typedef void (wxEvtHandler::*StatusBarEventFunction)(StatusBarEvent&);

BEGIN_DECLARE_EVENT_TYPES()
  DECLARE_EVENT_TYPE( myEVT_STATUSBAREVENT, 1 )
END_DECLARE_EVENT_TYPES()

#define EVT_STATUSBAREVENT(func) DECLARE_EVENT_TABLE_ENTRY( myEVT_STATUSBAREVENT, -1, -1, (wxObjectEventFunction)(StatusBarEventFunction) & func, (wxObject*)NULL),

#endif
