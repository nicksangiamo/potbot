#ifndef BN_SESSION_MANAGER_H
#define BN_SESSION_MANAGER_H

#include <list>
#include <wx/event.h>
#include <boost/asio.hpp>
#include "bncs_packet_manager.h"
#include "bnls_packet_manager.h"
#include "logger.h"
#include "potbot.h"
#include "potbot_frame.h"
#include "chat_window_event.h"

class BnSessionManager {
 public:
   BnSessionManager(Potbot* potbot);
   ~BnSessionManager();
   bool Init();
   bool BeginLogon(const std::string& username, const std::string& password);
   bool BeginLogon(const std::string& username, const std::string& password,
                   const std::string& cdkey, const std::string& first_channel);
   void EndSession();
   void SendChatMessage(std::string& message);
   bool LoggedOn();
   void set_bnls_connected(bool connected);
   void set_bncs_connected(bool connected);
   static void StaticSendChatMessage(BnSessionManager* session_manager,
                                     std::string message);
   static void StaticEndSession(BnSessionManager* session_manager);

 private:
   friend class BnlsPacketManager;
   friend class BncsPacketManager;

   typedef void (BnSessionManager::*chat_event_handler)(
       dword flags,
       dword ping,
       std::string username,
       std::string text);
   static const int kServerConnectTimeoutMs = 5000;

   Potbot* potbot_;
   BncsPacketManager* bncs_packet_manager_;
   BnlsPacketManager* bnls_packet_manager_;
   boost::asio::io_service* io_service_;
   Logger* logger_;

   bool bnls_connected_;
   bool bncs_connected_;
   bool logged_on_;
   byte ver_byte_;
   dword received_ping_;
   dword server_token_;
   dword client_token_;
   dword exe_hash_;
   dword hashed_key_data_[5];
   std::string exe_info_;
   std::string versison_filename_;
   dword dll_digit_;
   std::string checksum_formula_;
   std::string username_;
   std::string password_;
   std::string cdkey_;
   std::string channel_;
   std::string default_channel_;
   std::list<std::string> channel_list_;
   std::list<std::string> bn_default_channels_;
   std::map<int, chat_event_handler> chat_event_handlers_;

   void BnlsConnected();
   void BnlsConnectTimeout();
   void BncsConnected();
   void BncsConnectTimeout();

   void InitChatEventHandlers();
   bool LogChatWindowMessage(const std::string& message);
   void UpdateStatusBar(const std::string& status);
   
   void HandleBnlsAuthorize(BnlsPacket& authorize_packet);
   void HandleBnlsAuthorizeproof(BnlsPacket& authorizeproof_packet);
   void HandleBnlsVersioncheck(BnlsPacket& versioncheck_packet);
   void HandleBnlsRequestversionbyte(BnlsPacket& requestversionbyte_packet);
   void HandleBnlsCdkey(BnlsPacket& cdkey_packet);
   void HandleBnlsHashdata(BnlsPacket& hashdata_packet);

   void HandleSidPing(BncsPacket& ping_packet);
   void HandleSidAuthInfo(BncsPacket& auth_info_packet);
   void HandleSidAuthCheck(BncsPacket& auth_check_packet);
   void HandleSidLogonresponse2(BncsPacket& logonresponse2_packet);
   void HandleSidGetchannellist(BncsPacket& getchannellist_packet);
   void HandleSidEnterchat(BncsPacket& enterchat_packet);
   void HandleSidChatevent(BncsPacket& chatevent_packet);

   void HandleEidShowuser(dword flags, dword ping, std::string username,
                          std::string text);
   void HandleEidJoin(dword flags, dword ping, std::string username,
                      std::string text);
   void HandleEidLeave(dword flags, dword ping, std::string username,
                       std::string text);
   void HandleEidWhisper(dword flags, dword ping, std::string username,
                         std::string text);
   void HandleEidTalk(dword flags, dword ping, std::string username,
                      std::string text);
   void HandleEidBroadcast(dword flags, dword ping, std::string username,
                           std::string text);
   void HandleEidChannel(dword flags, dword ping, std::string username,
                         std::string text);
   void HandleEidUserflags(dword flags, dword ping, std::string username,
                           std::string text);
   void HandleEidWhispersent(dword flags, dword ping, std::string username,
                             std::string text);
   void HandleEidChannelfull(dword flags, dword ping, std::string username,
                             std::string text);
   void HandleEidChanneldoesnotexist(dword flags, dword ping,
                                     std::string username,
                                     std::string text);
   void HandleEidChannelrestricted(dword flags, dword ping,
                                   std::string username,
                                   std::string text);
   void HandleEidInfo(dword flags, dword ping, std::string username,
                      std::string text);
   void HandleEidError(dword flags, dword ping, std::string username,
                       std::string text);
   void HandleEidIgnore(dword flags, dword ping, std::string username,
                        std::string text);
   void HandleEidAccept(dword flags, dword ping, std::string username,
                        std::string text);
   void HandleEidEmote(dword flags, dword ping, std::string username,
                        std::string text);

   void WaitBnlsConnected();
   void WaitBncsConnected();

   static void WaitBnlsConnectedCallback(BnSessionManager* session_manager,
                                  Logger* logger,
                                  int recheck_interval_ms,
                                  boost::asio::deadline_timer* callback_timer,
                                  const boost::system::error_code& timer_error);
   static void WaitBncsConnectedCallback(BnSessionManager* session_manager,
                                  Logger* logger,
                                  int recheck_interval_ms,
                                  boost::asio::deadline_timer* callback_timer,
                                  const boost::system::error_code& timer_error);
   std::string ValidateChatMessage(std::string& message);
};

BEGIN_DECLARE_EVENT_TYPES()
  DECLARE_EVENT_TYPE( myEVT_CONNECTED, 1 )
  DECLARE_EVENT_TYPE( myEVT_DISCONNECTED, 2 )
END_DECLARE_EVENT_TYPES()

#endif
