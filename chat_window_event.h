#ifndef CHAT_WINDOW_EVENT_H
#define CHAT_WINDOW_EVENT_H

#include <wx/wx.h>

class ChatWindowEvent : public wxEvent {
 public:
   ChatWindowEvent(wxWindow* window = (wxWindow*)NULL,
               const wxString& message = "");
   //ChatWindowEvent(const wxString& message, wxWindow* window = (wxWindow*)NULL);
   wxEvent* Clone() const { return new ChatWindowEvent(*this); }
   wxString Message();
 private:
   wxString message_;

   DECLARE_DYNAMIC_CLASS(ChatWindowEvent)
};

typedef void (wxEvtHandler::*ChatWindowEventFunction)(ChatWindowEvent&);

BEGIN_DECLARE_EVENT_TYPES()
  DECLARE_EVENT_TYPE( myEVT_CHATWINDOWEVENT, 1 )
END_DECLARE_EVENT_TYPES()

#define EVT_CHATWINDOWEVENT(func) DECLARE_EVENT_TABLE_ENTRY( myEVT_CHATWINDOWEVENT, -1, -1, (wxObjectEventFunction)(ChatWindowEventFunction) & func, (wxObject*)NULL),

#endif
