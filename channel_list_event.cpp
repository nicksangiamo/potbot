#include "wx/wx.h"
#include "channel_list_event.h"
#include <list>
#include <string>
#include <algorithm>
#include <cstdio>

using std::list;
using std::string;

IMPLEMENT_DYNAMIC_CLASS(ChannelListEvent, wxEvent)
DEFINE_EVENT_TYPE(myEVT_CHANNELLISTEVENT)

ChannelListEvent::ChannelListEvent(wxWindow* window,
                                   const list<std::string>& channel_list) {
  SetEventType(myEVT_CHANNELLISTEVENT);
  SetEventObject(window);
  set_channel_list(channel_list);
}

/*
wxString ChannelListEvent::Username() {
  return username_;
}
*/

void ChannelListEvent::set_channel_list(const list<string>& channel_list) {
  channel_list_.clear();
  copy(channel_list.begin(), channel_list.end(), back_inserter(channel_list_));
}

list<wxString> ChannelListEvent::ChannelList() {
  return channel_list_;
}
