#ifndef BN_CONNECTION_H
#define BN_CONNECTION_H

#include <list>
#include <string>
#include <iostream>
#include <boost/asio.hpp>
#include <boost/array.hpp>
#include "bn_packet.h"
#include "logger.h"

class BnPacketManager;

class BnConnection {
 public:
   enum ConnectionTypes {
     kBnlsConnection = 0,
     kBncsConnection = 1
   };
   BnConnection();
   BnConnection(const ConnectionTypes& connection_type,
                const std::string& ip_address, const std::string& port,
                BnPacketManager* packet_manager,
                boost::asio::io_service* io_service);
   bool Init();
   void SendPacket(BnPacket& bn_packet);
   void SendGameProtocolByte();

 private:
   BnPacketManager* packet_manager_;
   std::string ip_address_;
   std::string port_;
   boost::asio::io_service* io_service_;
   boost::asio::ip::tcp::resolver* resolver_;
   boost::asio::ip::tcp::socket* sock_;
   boost::array<byte, 4096> buffer_;
   Logger* logger_;
   static void ResolveHandler(
       const boost::system::error_code& error_code,
       boost::asio::ip::tcp::resolver::iterator resolver_iterator,
       BnConnection* bn_connection);
   static void ConnectHandler(const boost::system::error_code& error_code,
                               BnConnection* bn_connection);
   static void ReadHandler(const boost::system::error_code &ec,
                            std::size_t num_bytes_read,
                            BnConnection* bn_connection);
   void set_ip_address(std::string ip_address);
   std::string GetSocketString();

};

#endif
