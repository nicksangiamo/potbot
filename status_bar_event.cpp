#include "wx/wx.h"
#include "status_bar_event.h"

IMPLEMENT_DYNAMIC_CLASS(StatusBarEvent, wxEvent)
DEFINE_EVENT_TYPE(myEVT_STATUSBAREVENT)

StatusBarEvent::StatusBarEvent(wxWindow* window, const wxString& status) {
  SetEventType(myEVT_STATUSBAREVENT);
  SetEventObject(window);
  status_ = status;
}

wxString StatusBarEvent::Status() {
  return status_;
}
