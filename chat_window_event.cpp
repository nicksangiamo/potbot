#include "wx/wx.h"
#include "chat_window_event.h"

IMPLEMENT_DYNAMIC_CLASS(ChatWindowEvent, wxEvent)
DEFINE_EVENT_TYPE(myEVT_CHATWINDOWEVENT)

ChatWindowEvent::ChatWindowEvent(wxWindow* window, const wxString& message) {
  SetEventType(myEVT_CHATWINDOWEVENT);
  SetEventObject(window);
  message_ = message;
}

wxString ChatWindowEvent::Message() {
  return message_;
}
