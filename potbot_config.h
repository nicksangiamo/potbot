#ifndef POTBOT_CONFIG_H
#define POTBOT_CONFIG_H

#include <string>
#include <boost/property_tree/ptree.hpp>

class PotbotConfig {
 public:
   PotbotConfig(const std::string& filename);
   std::string Get(const std::string& attribute_path);
   bool Set(const std::string& attribute_path, const std::string& value);

 private:
   void load(const std::string& filename);
   boost::property_tree::ptree config_ptree_;
};

#endif
