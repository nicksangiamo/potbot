#include "logger.h"
#include <iostream>
#include <string>
//#include <boost/log/trivial.hpp>

using std::string;
using std::cout;
using std::endl;

Logger::
Logger()
{
    prepend = "";
}

Logger::
Logger(string prepend)
{
    this->prepend = prepend;
}

void
Logger::
write_prepend()
{
    if(prepend != "")
    {
        cout << prepend << ": ";
    }
}

void
Logger::
Log(string msg)
{
    write_prepend();
    cout << msg << endl;
}

void
Logger::
LogInt(string int_name, int the_int)
{
    write_prepend();
    cout << int_name << ": " << the_int << endl;
}

void
Logger::
log_string(string string_name, string the_string)
{
    write_prepend();
    cout << string_name << ": " << the_string << endl;
}
