#include <list>
#include <cstdlib>
#include <iostream>
#include <sstream>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/algorithm/string.hpp>
#include "bn_session_manager.h"
#include "bn_connection.h"
#include "bnls_ids.h"
#include "bnls_packet.h"
#include "bnls_packet_manager.h"

using std::string;
using std::stringstream;
using std::list;

constexpr int BnlsPacket::kValidIds[];

BnlsPacketManager::BnlsPacketManager(BnSessionManager* session_manager,
                                     boost::asio::io_service* io_service) {
  set_io_service(io_service);
  set_connection(new BnConnection(BnConnection::kBnlsConnection,
                 "23.92.212.162", "9367", this, io_service_));
  set_logger(new Logger("BnlsPacketManager"));
  set_session_manager(session_manager);
}

BnlsPacketManager::BnlsPacketManager(string hostname,
                                     BnSessionManager* session_manager,
                                     boost::asio::io_service* io_service) {
  set_io_service(io_service);
  set_connection(new BnConnection(BnConnection::kBnlsConnection,
                 hostname, "9367", this, io_service_));
  set_logger(new Logger("BnlsPacketManager"));
  set_session_manager(session_manager);
}

bool BnlsPacketManager::Init() {
  InitPacketHandlers();
  return BnPacketManager::Init();
}

void BnlsPacketManager::Connected() {
  session_manager_->set_bnls_connected(true);
}

void BnlsPacketManager::HandlePacket(byte* packet_data,
                                      packet_size_t packet_length) {
  //TODO: check if this is valid data
  BnlsPacket bnls_packet(packet_data, packet_length);
  logger_->Log("handling packet...");
  byte id = bnls_packet.id();
  if(packet_handlers_.count(id)) {
    (session_manager_->*packet_handlers_[id])(bnls_packet);
  } else {
    stringstream ss;
    ss << "Unknown packet id: " << id;
    logger_->Log(ss.str());
  }
}

void BnlsPacketManager::SendBnlsPacket(BnlsPacket& bnls_packet) {
  connection_->SendPacket(bnls_packet);
}

list<RawBnPacket> BnlsPacketManager::ExtractRawPackets(byte* data,
                                                       int num_bytes) {
  list<RawBnPacket> raw_packets;
  for(int byte_index=0; byte_index < num_bytes-BnlsPacket::kBnlsHeaderLen;
      byte_index++) {
    byte* packet_length_ptr = data + byte_index;
    packet_size_t packet_length = (packet_length_ptr[0] << 0) |
                                  (packet_length_ptr[1] << 8);
    byte packet_id = data[byte_index + sizeof(packet_length)];
    printf("len: %04X, id: %02X\n", packet_length, packet_id);
    if(byte_index+packet_length > num_bytes) {
      continue;
    }
    for(int i=0; i<sizeof(BnlsPacket::kValidIds); i++) {
      if(BnlsPacket::kValidIds[i] == packet_id) {
        struct RawBnPacket raw_packet = {data+byte_index, packet_length};
        raw_packets.push_back(raw_packet);
        byte_index += packet_length;
      }
    }
  }
  return raw_packets;
}

void BnlsPacketManager::SendAuthorize() {
  logger_->Log("Sending BNLS_AUTHORIZE");
  BnlsPacket authorize_packet(BNLS_AUTHORIZE);
  authorize_packet.AddString("stealth");
  SendBnlsPacket(authorize_packet);
}

void BnlsPacketManager::SendAuthorizeproof() {
  logger_->Log("Sending BNLS_AUTHORIZEPROOF");
  BnlsPacket authorizeproof_packet(BNLS_AUTHORIZEPROOF);
  authorizeproof_packet.AddDword(0xd3c9e94b); //dummy checksum
  SendBnlsPacket(authorizeproof_packet);
}

void BnlsPacketManager::SendRequestversionbyte() {
  logger_->Log("Sending BNLS_REQUESTVERSIONBYTE");
  BnlsPacket requestversionbyte_packet(BNLS_REQUESTVERSIONBYTE);
  requestversionbyte_packet.AddDword(0x00000002); // SC product code
  SendBnlsPacket(requestversionbyte_packet);
}

void BnlsPacketManager::SendVersioncheck(dword dll_digit,
                                          string checksum_formula) {
  logger_->Log("Sending BNLS_VERSIONCHECK");
  //(DWORD) Product ID
  //(DWORD) Version DLL digit
  //(STRING) Checksum formula
  BnlsPacket versioncheck_packet(BNLS_VERSIONCHECK);
  versioncheck_packet.AddDword(0x00000002);
  versioncheck_packet.AddDword(dll_digit);
  versioncheck_packet.AddString(checksum_formula);
  versioncheck_packet.Log(logger_);
  SendBnlsPacket(versioncheck_packet);
}

void BnlsPacketManager::SendCdkey(dword server_token, std::string cd_key) {
  logger_->Log("Sending BNLS_CDKEY");
  BnlsPacket cdkey_packet(BNLS_CDKEY);
  cdkey_packet.AddDword(server_token);
  cdkey_packet.AddString(cd_key);
  cdkey_packet.Log(logger_);
  SendBnlsPacket(cdkey_packet);
}

void BnlsPacketManager::SendHashdata(string password, dword server_token,
                                      dword client_token) {
  logger_->Log("Sending BNLS_HASHDATA");
  BnlsPacket hashdata_packet(BNLS_HASHDATA);
  boost::to_upper(password);
  hashdata_packet.AddDword(password.size());
  hashdata_packet.AddDword(0x00000002); //Indicates double hash
  hashdata_packet.AddString(password);
  hashdata_packet.AddDword(client_token);
  hashdata_packet.AddDword(server_token);
  SendBnlsPacket(hashdata_packet);
}

void BnlsPacketManager::InitPacketHandlers() {
  packet_handlers_[BNLS_AUTHORIZE] = &BnSessionManager::HandleBnlsAuthorize;
  packet_handlers_[BNLS_AUTHORIZEPROOF] =
    &BnSessionManager::HandleBnlsAuthorizeproof;
  packet_handlers_[BNLS_VERSIONCHECK] =
    &BnSessionManager::HandleBnlsVersioncheck;
  packet_handlers_[BNLS_REQUESTVERSIONBYTE] =
    &BnSessionManager::HandleBnlsRequestversionbyte;
  packet_handlers_[BNLS_CDKEY] = &BnSessionManager::HandleBnlsCdkey;
}
