#include "bnls_packet.h"
#include <iostream>
#include <cstring>

using std::vector;
using std::cout;
using std::endl;

bool BnlsPacket::IsValidPacket(byte* data, int data_length) {
    packet_size_t bnls_length_word = *((packet_size_t*)data);
    if(data_length != bnls_length_word || data_length < kBnlsHeaderLen) {
        return false;
    }
    return true;
}

BnlsPacket::BnlsPacket() {
  set_type(kBnlsPacket);
}

BnlsPacket::BnlsPacket(byte id)
    : BnPacket(kBnlsPacket, id, kBnlsHeaderLen) {}

BnlsPacket::BnlsPacket(byte* data, packet_size_t size)
    : BnPacket(data, size) {
  set_type(kBnlsPacket);
  set_length(kBnlsHeaderLen);
  set_id(data+2);
  for(packet_size_t i=3; i<size; i++) {
    AddByte(data[i]);
  }
}

byte* BnlsPacket::ToBuffer() {
  byte id = this->id();
  packet_size_t length = this->length();

  byte* buffer = new byte[length];
  int buffer_index = 0;
  memcpy(buffer, &length, sizeof(length));
  buffer_index += sizeof(length);
  memcpy(buffer+buffer_index, &id, sizeof(id));
  buffer_index += sizeof(id);
  vector<byte>::const_iterator iter;
  for(byte byte : data()) {
    buffer[buffer_index++] = byte;
  }
  return buffer;
}

packet_size_t BnlsPacket::GetDataLength() {
  return length() - kBnlsHeaderLen;
}
