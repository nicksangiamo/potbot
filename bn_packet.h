#ifndef BN_PACKET_H
#define BN_PACKET_H

#include <vector>
#include <stdint.h>
#include "logger.h"

typedef uint32_t dword;
typedef uint8_t byte;
typedef uint16_t packet_size_t;

class BnPacketIterator;

class BnPacket {
 public:
   enum BnPacketTypes {
     kBnlsPacket = 0,
     kBncsPacket = 1
   };

   BnPacket();
   BnPacket(byte id);
   BnPacket(byte* data, packet_size_t length);
   BnPacket(BnPacketTypes type, byte id, packet_size_t length);

   void set_length(packet_size_t length);
   void set_id(byte* data);
   void set_id(byte id);
   void set_length(byte* data);
   void set_type(BnPacketTypes type);

   packet_size_t length() const;
   byte id() const;
   byte* data_pointer();
   void set_data(const std::vector<byte>&);
   const std::vector<byte> data() const;

   byte ExtractByte(packet_size_t position);
   dword ExtractDword(packet_size_t position);
   dword* ExtractDwordArray(packet_size_t position, packet_size_t num_dwords,
                                 dword* dword_data);
   std::string ExtractString(packet_size_t position);

   virtual byte* ToBuffer() = 0;
   BnPacketIterator GetIterator();

   void Log(Logger* logger);
   void AddByte(byte byte);
   void AddDword(dword dword);
   void AddBigEndianDword(dword dword);
   void AddLittleEndianDword(dword dword);
   void AddDwordArray(dword* dword_data, int num_dwords);
   void AddString(std::string the_string);
   virtual packet_size_t GetDataLength() = 0;

   static bool IsValidPacket(byte* data, packet_size_t length);
   static void LogData(byte* data, packet_size_t data_size, Logger* logger);

 private:
   byte id_;
   packet_size_t length_;
   std::vector<byte> data_;
   BnPacketTypes type_;
};   

class BnPacketIterator {
 public:
   BnPacketIterator();
   BnPacketIterator(BnPacket* packet);
   byte GetNextByte();
   dword GetNextDword();
   std::string GetNextString();
   int RemainingStrings();
   bool HasAnotherString();
 private:
   BnPacket* packet_;
   int byte_index_;
};

struct RawBnPacket {
  byte* buffer;
  packet_size_t size; 
};

#endif
