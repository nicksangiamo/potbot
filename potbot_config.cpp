#include "potbot_config.h"
#include <string>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/property_tree/exceptions.hpp>

using std::string;
using boost::property_tree::ptree_error;

PotbotConfig::PotbotConfig(const string& filename) {
  load(filename);
}

string PotbotConfig::Get(const string& attribute_path) {
  string value;
  try {
    value =  config_ptree_.get<string>(attribute_path);
  } catch(ptree_error ptree_error) {
    value = "";
  }
  return value;
}

void PotbotConfig::load(const string& filename) {
  read_ini(filename, config_ptree_);
  return;
}
