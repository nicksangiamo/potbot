#ifndef BN_PACKET_MANAGER_H
#define BN_PACKET_MANAGER_H

#include <string>
#include <boost/asio.hpp>
#include "bn_packet.h"
#include "bn_connection.h"
#include "logger.h"

class BnSessionManager;

class BnPacketManager {
 public:
   virtual bool Init() = 0;
   virtual void HandlePacket(byte* data, packet_size_t length) = 0;
   virtual void Connected() = 0;
   void SendGameProtocolByte();
 
   void set_connection(BnConnection* connection);
   void set_session_manager(BnSessionManager* session_manager);
   void set_logger(Logger* logger);
   void set_io_service(boost::asio::io_service* io_service);

   virtual std::list<RawBnPacket> ExtractRawPackets(byte* data, int num_bytes) = 0;

 protected:
   friend class BnConnection;
   BnConnection* connection_;
   BnSessionManager* session_manager_;
   Logger* logger_;
   boost::asio::io_service* io_service_;
};

#endif
