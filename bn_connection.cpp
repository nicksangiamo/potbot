#include <list>
#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include "bn_packet.h"
#include "bn_packet_manager.h"
#include "logger.h"
#include "bn_connection.h"

using std::string;
using std::list;

string resolve_success_message = "Address resolved successfully, now attempting to connect";

BnConnection::BnConnection() {}

BnConnection::BnConnection(const BnConnection::ConnectionTypes& connection_type,
                           const string& ip_address, const string& port,
                           BnPacketManager* packet_manager,
                           boost::asio::io_service* io_service)
    : ip_address_(ip_address),
      port_(port),
      packet_manager_(packet_manager),
      io_service_(io_service) {
  switch(connection_type) {
    case BnConnection::kBnlsConnection: {
      logger_ = new Logger("BnlsConnection");
      break;
    }
    case BnConnection::kBncsConnection: {
      logger_ = new Logger("BncsConnection");
      break;
    }
  }
}

bool BnConnection::Init() {
  logger_->Log("Initializing attempt to connect to " + GetSocketString()
      + "...");
  boost::asio::ip::tcp::resolver::query query(ip_address_, port_);
  sock_ = new boost::asio::ip::tcp::socket(*io_service_);
  resolver_ = new boost::asio::ip::tcp::resolver(*io_service_);
  logger_->Log("Attempting to resolve server address...");
  resolver_->async_resolve(query, boost::bind(&BnConnection::ResolveHandler,
                           boost::asio::placeholders::error,
                           boost::asio::placeholders::iterator, this));
  return true;
}

void BnConnection::ReadHandler(const boost::system::error_code &error_code,
                                std::size_t num_bytes_read,
                                BnConnection* bn_connection) {
  if(!error_code) {
    bn_connection->logger_->Log("Received some data");
    bn_connection->logger_->LogInt("num_bytes_read", num_bytes_read);
    byte* read_data = bn_connection->buffer_.data();
    BnPacket::LogData(read_data, num_bytes_read, bn_connection->logger_);
    list<RawBnPacket> received_packets = bn_connection->packet_manager_->
        ExtractRawPackets(read_data, num_bytes_read);
    bn_connection->logger_->LogInt("num_received_packets",
                                    received_packets.size());
    for(RawBnPacket& raw_packet : received_packets) {
      bn_connection->packet_manager_->HandlePacket(raw_packet.buffer,
                                                   raw_packet.size);
    }
    bn_connection->logger_->Log("Starting async read");
    bn_connection->sock_->async_read_some(
        boost::asio::buffer(bn_connection->buffer_),
        boost::bind(&BnConnection::ReadHandler,
        boost::asio::placeholders::error,
        boost::asio::placeholders::bytes_transferred, bn_connection));
  } else {
      bn_connection->logger_->Log("Read error: " + error_code.message());
      bn_connection->logger_->Log("Read error: " + std::to_string(error_code.value()));
  }
}

void BnConnection::ResolveHandler(
    const boost::system::error_code &error_code,
    boost::asio::ip::tcp::resolver::iterator resolver_iterator,
    BnConnection* bn_connection) {
    if(!error_code) {
      bn_connection->logger_->Log(resolve_success_message);
      bn_connection->sock_->async_connect(
          *resolver_iterator,
          boost::bind(&BnConnection::ConnectHandler,
                      boost::asio::placeholders::error, bn_connection));
    } else {
        bn_connection->logger_->Log("Resolve error: " + error_code.message());
    }
}

void BnConnection::ConnectHandler(const boost::system::error_code& error_code,
                                   BnConnection* bn_connection) {
  if(!error_code)
  {
    bn_connection->logger_->Log("Connect successful!");
    bn_connection->packet_manager_->Connected();
    bn_connection->logger_->Log("Starting async read");
    bn_connection->sock_->async_read_some(boost::asio::buffer(
          bn_connection->buffer_), boost::bind(&BnConnection::ReadHandler,
          boost::asio::placeholders::error, 
          boost::asio::placeholders::bytes_transferred, bn_connection));
  } else {
    bn_connection->logger_->Log("Connect error: " + error_code.message());
  }
}

/*
list<RawBnPacket> BnConnection::ExtractRawPackets(byte* data, int num_bytes) {
  list<RawBnPacket> raw_packets;
  for(int byte_index=0; byte_index<num_bytes; byte_index++) {
    byte curr_byte = data[byte_index];
    if(curr_byte == BnPacket::kBnlsPacket)
  }
  return raw_packets;
}
*/

void BnConnection::SendPacket(BnPacket& bn_packet) {
  int size = bn_packet.length();
  byte* data = bn_packet.ToBuffer();
  boost::asio::write(*sock_, boost::asio::buffer(data, size));
  delete[] data;
}

void BnConnection::SendGameProtocolByte()
{
  byte game_protocol_byte = 0x01;
  logger_->Log("Sending game protocol byte");
  boost::asio::write(*sock_, boost::asio::buffer(&game_protocol_byte, 1));
}

string BnConnection:: GetSocketString() {
  return ip_address_ + ":" + port_;
}
