#include "bncs_packet.h"
#include <iostream>
#include <cstring>

using std::vector;
using std::cout;
using std::endl;

bool BncsPacket::IsValidPacket(byte* data, packet_size_t length) {
  if(length < kBncsHeaderLen || data[0] < kBncsHeaderLen) {
    return false;
  }
  if(data[0] != kBncsFirstByte) {
    return false;
  }
  return true;
}

BncsPacket::BncsPacket() : BnPacket(kBncsPacket, 0, kBncsHeaderLen) {}

BncsPacket::BncsPacket(byte id)
    : BnPacket(kBncsPacket, id, kBncsHeaderLen) {}

BncsPacket::BncsPacket(byte* data, packet_size_t length)
{
  set_type(kBncsPacket);
  set_id(data+1);
  set_length(kBncsHeaderLen);

  for(int i=kBncsHeaderLen; i<length; i++) {
    AddByte(data[i]);
  }
}

byte* BncsPacket::ToBuffer() {
  byte id = this->id();
  packet_size_t length = this->length();

  byte* buffer = new byte[length];
  int buffer_index = 0;
  buffer[buffer_index++] = kBncsFirstByte;
  memcpy(buffer+buffer_index, &id, sizeof(id));
  buffer_index += sizeof(id);
  memcpy(buffer+buffer_index, &length, sizeof(length));
  buffer_index += sizeof(length);
  vector<byte>::const_iterator iter;

  //for(iter = data_.begin(); iter != data_.end(); iter++) {
  for(byte byte : data()) {
    buffer[buffer_index++] = byte;
  }

  return buffer;
}

packet_size_t BncsPacket::GetDataLength() {
  return length() - kBncsHeaderLen;
}
