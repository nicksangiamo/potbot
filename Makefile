CC=g++
STD=-std=c++11
INC=-I/usr/include/
LIBS=-lpthread -lboost_system -lboost_thread -lboost_date_time
SRC=$(wildcard *.cpp)
OBJ=$(SRC:.cpp=.o)
CFLAGS=-c $(INC) $(STD)
LDFLAGS=$(STD) $(LIBS)
EXEC=potbot

all: $(EXEC)

$(EXEC):	$(OBJ)
	$(CC) -o$@ $^ $(LDFLAGS) `wx-config --libs`

%.o: %.cpp %.h
	$(CC) `wx-config --cxxflags` $(CFLAGS) -o$@ $<

clean:
	rm -f $(OBJ) $(EXEC)
