#include <iostream>
#include <list>
#include <boost/asio.hpp>
#include "bn_packet_manager.h"

using std::string;
using std::list;

bool BnPacketManager::Init() {
    return connection_->Init();
}

void BnPacketManager::SendGameProtocolByte() {
    connection_->SendGameProtocolByte();
}

void BnPacketManager::set_connection(BnConnection* connection) {
  connection_ = connection;
}

void BnPacketManager::set_session_manager(BnSessionManager* session_manager) {
  session_manager_ = session_manager;
}

void BnPacketManager::set_logger(Logger* logger) {
  logger_ = logger;
}

void BnPacketManager::set_io_service(boost::asio::io_service* io_service) {
  io_service_ = io_service;
}
