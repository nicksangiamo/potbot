#ifndef POTBOT_H
#define POTBOT_H

#include <wx/wxprec.h>
#ifndef WX_PRECOMP
  #include <wx/wx.h>
#endif
#include <string>
#include <boost/asio.hpp>
#include <boost/thread.hpp>
#include "potbot_config.h"

class PotbotFrame;
class BnSessionManager;

class Potbot: public wxApp {
 public:
   virtual bool OnInit();
   boost::asio::io_service* IoService();
   void Connect();
   void Disconnect();
   PotbotFrame* Frame();
   BnSessionManager* SessionManager();
 private:
   boost::asio::io_service* io_service_;
   boost::thread* bn_session_thread_;
   PotbotFrame* frame_;
   BnSessionManager* session_manager_;
   PotbotConfig* config_;
   std::string config_filename_ = "config.ini";
   bool in_session_ = false;
   void InitConnect();
};

DECLARE_APP(Potbot)

#endif
